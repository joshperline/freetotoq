package helloworld.cs160.berkeley.edu.freetotoq;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.qualcomm.toq.smartwatch.api.v1.deckofcards.Constants;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.DeckOfCardsManager;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteDeckOfCardsException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Random;

import helloworld.cs160.berkeley.edu.flickrj.sample.android.FlickrjActivity;

/**
 * Created by joshperline on 10/10/14.
 */
public class DrawingActivity extends Activity {

    private SeekBar redBar, greenBar, blueBar, sizeBar;

    private int sProg, rProg, gProg, bProg;

    public boolean initDone;

    int strokeColor;
    Bitmap canvasBitmap;
    Canvas canvas;

    public HashMap<String, barListener> seekerToListener;

    boolean firstLoop = true;
    int maxTextWidth;

    /** Width and Height of phone screen in dp. */
    float dpHeight, dpWidth;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.drawing_main);

        calculateWindowSize();

        initProgressBars();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {

            // Save the state of the current drawing when going to settings
            canvasBitmap = DrawingView.canvasBitmap;

            // Switch and init new View
            setContentView(R.layout.settings_main);

            initDone = false;

            seekerToListener = new HashMap<String, barListener>();

            sizeBar = (SeekBar)findViewById(R.id.sizeBar); // make seekbar object
            seekerToListener.put("sizeBar", new barListener(false));
            sizeBar.setOnSeekBarChangeListener( seekerToListener.get("sizeBar") ); // set seekbar listener.
            sizeBar.setProgress( sProg );

            redBar = (SeekBar)findViewById(R.id.redBar);
            seekerToListener.put("redBar", new barListener(true));
            redBar.setOnSeekBarChangeListener( seekerToListener.get("redBar") );
            redBar.setProgress( rProg );

            blueBar = (SeekBar)findViewById(R.id.blueBar);
            seekerToListener.put("blueBar", new barListener(true));
            blueBar.setOnSeekBarChangeListener( seekerToListener.get("blueBar") );
            blueBar.setProgress( bProg );

            greenBar = (SeekBar)findViewById(R.id.greenBar);
            seekerToListener.put("greenBar", new barListener(true));
            greenBar.setOnSeekBarChangeListener( seekerToListener.get("greenBar") );
            greenBar.setProgress( gProg );

            initDone = true;

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void backFromSettings(View view) {
        int size = seekerToListener.get("sizeBar").getSeekerValue();

        int color = strokeColor;

        setContentView( R.layout.drawing_main );

        DrawingView.comingFromSettings = true;
        DrawingView.setCanvasBitmap( canvasBitmap );
        DrawingView.setStrokeSize( size );
        DrawingView.setStrokeColor( color );

        centerUploadButton( false );
    }

    @Override
    public void onWindowFocusChanged( boolean hasFocus ) {
        super.onWindowFocusChanged( hasFocus );

        if ( firstLoop ) {

            centerUploadButton(true);

        }
    }

    void centerUploadButton(boolean firstTime) {
        Button tog = (Button) findViewById( R.id.toggle );
        Button uploadButton = (Button) findViewById( R.id.upload );
        LinearLayout.LayoutParams params1 = (LinearLayout.LayoutParams) uploadButton.getLayoutParams();

        if ( firstTime ) {
            firstLoop = false;

            int buf1 = (int) (dpWidth / 2.0f) - tog.getWidth() - (uploadButton.getWidth() / 2);
            params1.setMargins(0, 0, buf1, 0); //substitute parameters for left, top, right, bottom
            uploadButton.setLayoutParams(params1);

            maxTextWidth = buf1;
        } else {
            params1.setMargins(0, 0, maxTextWidth, 0);
        }
    }

    /************* DRAWING *************/

    public void toggleEraser(View view) {
        if ( ( (ToggleButton) view ).isChecked() )
        {
            DrawingView.switchMode(true);
        }
        else
        {
            DrawingView.switchMode(false);
        }
    }

    public void updateBrushThickness() {
        sProg = seekerToListener.get("sizeBar").getSeekerValue();
        DrawingView.setStrokeSize( sProg );
    }

    /************* FLICKR *************/


    public void uploadDrawing(View view)
    {
        String fileName = Environment.getExternalStorageDirectory() + createPicturePath();
        try
        {
            /* Write bitmap to file using JPEG or PNG and 90% quality hint for JPEG. */
            OutputStream stream = new FileOutputStream(fileName);
            DrawingView.canvasBitmap.compress(Bitmap.CompressFormat.PNG, 90, stream);
            stream.close();

            addPicToGallery(fileName);

            /** Upload Picture to Flickr. */
            Intent intent = new Intent();
            intent = new Intent(getApplicationContext(), FlickrjActivity.class);
            intent.putExtra("flickImagePath", fileName);
            startActivity(intent);

            Toast.makeText(getApplicationContext(), "Save successful!", Toast.LENGTH_LONG).show ();

        }
        catch (Throwable ex)
        {
            Toast.makeText (getApplicationContext(), "Error: " + ex.getMessage(), Toast.LENGTH_LONG).show ();
            ex.printStackTrace ();
        }
    }

    private void addPicToGallery(String mCurrentPhotoPath) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }

    private String createPicturePath() {
        Random generator = new Random();
        int hash = generator.nextInt(10000);
        return "/fsm" + Integer.toString(hash) + ".png";
    }

    private void calculateWindowSize() {
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics ();
        display.getMetrics(outMetrics);

        float density  = getResources().getDisplayMetrics().density;
        dpHeight = outMetrics.heightPixels / density;
        dpWidth  = outMetrics.widthPixels / density;
    }


    public void updateRectangleColor() {
        GradientDrawable rect = (GradientDrawable)
                                findViewById( R.id.colorSwatch ).getBackground();

        rProg = seekerToListener.get("redBar").getSeekerValue();
        gProg = seekerToListener.get("greenBar").getSeekerValue();
        bProg = seekerToListener.get("blueBar").getSeekerValue();

        int color = Color.argb( 255, rProg, gProg, bProg );

        rect.setColor( color );

        strokeColor = color;
    }

    private void initProgressBars() {
        sProg = 8;
        rProg = 0;
        gProg = 0;
        bProg = 0;
    }

    public class barListener implements SeekBar.OnSeekBarChangeListener {
        private int seekerValue;

        private boolean isColorSwitcher;

        int getSeekerValue() {
            return seekerValue;
        }

        barListener(boolean colorSwitcher) {
            seekerValue = 0;
            isColorSwitcher = colorSwitcher;
        }

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress,
                                      boolean fromUser) {
            seekerValue = progress;
            if (isColorSwitcher && initDone) {
                updateRectangleColor();
            }
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {}

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            // TODO Auto-generated method stub
            seekBar.setSecondaryProgress(seekBar.getProgress()); // set the shade of the previous value.
        }
    }

}
