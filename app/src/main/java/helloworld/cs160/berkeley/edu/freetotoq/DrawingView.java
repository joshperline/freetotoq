package helloworld.cs160.berkeley.edu.freetotoq;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.AttributeSet;
import android.view.View;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.view.MotionEvent;


/**
 * Created by Josh Perline on 10/8/14.
 */
public class DrawingView extends View {
    // Paint
    private static Paint drawPaint;
    // Holds draw calls
    public static Canvas drawCanvas;
    // Bitmap to hold pixels
    public static Bitmap canvasBitmap;
    //previous spots
    private float startX = Float.MAX_VALUE;
    private float startY = Float.MAX_VALUE;

    private static int paintColor;
    private static int prevColor;
    private final int DEFAULT_SIZE = 8;

    public static boolean comingFromSettings = false;

    public DrawingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setupDrawing();
    }

    private void setupDrawing() {
        paintColor = 0xFF000000;
        prevColor = 0xFF000000;

        // Setup the drawing area for user interaction
        drawPaint = new Paint();
        // Change the color of paint to use

        drawPaint.setColor(paintColor);
        drawPaint.setStyle(Paint.Style.FILL_AND_STROKE );

        drawPaint.setStrokeWidth( DEFAULT_SIZE );
    }

    public static void switchMode(boolean erase) {
        if (erase)
        {
            /** Pen is now the same color as the background. */
            prevColor = paintColor;
            paintColor = 0xFFFFFFFF;
            drawPaint.setColor(paintColor);
        }
        else
        {
            paintColor = prevColor;
            drawPaint.setColor(paintColor);
        }
    }

    public static void setStrokeSize(int size) {
        drawPaint.setStrokeWidth( size );
    }

    public static void setStrokeColor(int color) {
        drawPaint.setColor( color );
    }

    public static void setCanvasBitmap(Bitmap b) {
        canvasBitmap = b;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawBitmap(canvasBitmap, 0, 0, drawPaint);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        // Make a new bitmap that stores each pixel on 4 bytes

        if (!comingFromSettings) {
            canvasBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
            drawCanvas = new Canvas(canvasBitmap);
            drawCanvas.drawARGB(255, 255, 255, 255);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float touchX = event.getX();
        float touchY = event.getY();

        switch (event.getAction()) {
            // User touches screen
            case MotionEvent.ACTION_DOWN:
                drawCanvas.drawLine(touchX - 0.01f, touchY - 0.01f, touchX, touchY, drawPaint);
                break;
            // User drags screen
            case MotionEvent.ACTION_MOVE:
                if ( startX != Float.MAX_VALUE && startY != Float.MAX_VALUE )
                {
                    drawCanvas.drawLine(startX, startY, touchX, touchY, drawPaint);
                }
                else
                {
                    drawCanvas.drawLine(touchX - 0.001f, touchY - 0.001f, touchX, touchY, drawPaint);
                }
                startX = touchX;
                startY = touchY;
                break;
            case MotionEvent.ACTION_UP:
                // reset the startX/Y
                startX = Float.MAX_VALUE;
                startY = Float.MAX_VALUE;
                break;
            default:
                return false;
        }
        invalidate();
        return true;
    }

}

