package helloworld.cs160.berkeley.edu.freetotoq;

import android.app.Activity;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.shapes.Shape;
import android.os.Bundle;
import android.view.View;
import android.widget.SeekBar;

import java.util.HashMap;

/**
 * Created by joshperline on 10/11/14.
 */
public class SettingsActivity extends Activity {

    private SeekBar redBar, greenBar, blueBar, sizeBar;

    public HashMap<String, barListener> seekerToListener;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.settings_main);

        seekerToListener = new HashMap<String, barListener>();

        sizeBar = (SeekBar)findViewById(R.id.sizeBar); // make seekbar object
        seekerToListener.put("sizeBar", new barListener(false));
        sizeBar.setOnSeekBarChangeListener( seekerToListener.get("sizeBar") ); // set seekbar listener.

        redBar = (SeekBar)findViewById(R.id.redBar);
        seekerToListener.put("redBar", new barListener(true));
        redBar.setOnSeekBarChangeListener( seekerToListener.get("redBar") );

        blueBar = (SeekBar)findViewById(R.id.blueBar);
        seekerToListener.put("blueBar", new barListener(true));
        blueBar.setOnSeekBarChangeListener( seekerToListener.get("blueBar") );

        greenBar = (SeekBar)findViewById(R.id.greenBar);
        seekerToListener.put("greenBar", new barListener(true));
        greenBar.setOnSeekBarChangeListener( seekerToListener.get("greenBar") );

    }

    public void updateRectangleColor() {
//        String rHex = Integer.toHexString(r);
//        String gHex = Integer.toHexString(g);
//        String bHex = Integer.toHexString(b);
//        String color = "0x" + rHex + gHex + bHex;

        GradientDrawable rect = (GradientDrawable)
                                findViewById( R.id.colorSwatch ).getBackground();
        int[] color = {255,
                       seekerToListener.get("redBar").getSeekerValue(),
                       seekerToListener.get("greenBar").getSeekerValue(),
                       seekerToListener.get("blueBar").getSeekerValue() };
        rect.setColors( color);
    }

    public void updateBrushThickness() {
        DrawingView.setStrokeSize( seekerToListener.get("sizeBar").getSeekerValue() );
    }

    public class barListener implements SeekBar.OnSeekBarChangeListener {

        private int seekerValue;

        private boolean isColorSwitcher;

        int getSeekerValue() {
            return seekerValue;
        }

        barListener(boolean colorSwitcher) {
            seekerValue = 0;
            isColorSwitcher = colorSwitcher;
        }

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress,
                                      boolean fromUser) {
            seekerValue = progress;
            if (isColorSwitcher) {
                updateRectangleColor();
            }
            else {
                updateBrushThickness();
            }
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
//            textAction.setText("starting to track touch");

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            // TODO Auto-generated method stub
            seekBar.setSecondaryProgress(seekBar.getProgress()); // set the shade of the previous value.
//            textAction.setText("ended tracking touch");
        }

    }

}
