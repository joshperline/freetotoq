package helloworld.cs160.berkeley.edu.freetotoq;

import helloworld.cs160.berkeley.edu.flickrj.sample.android.FlickrHelper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;
import android.app.AlertDialog;
import android.content.DialogInterface;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;

import com.googlecode.flickrjandroid.Flickr;
import com.googlecode.flickrjandroid.FlickrException;
import com.googlecode.flickrjandroid.REST;
import com.googlecode.flickrjandroid.photos.Photo;
import com.googlecode.flickrjandroid.photos.PhotoList;
import com.googlecode.flickrjandroid.photos.PhotosInterface;
import com.googlecode.flickrjandroid.photos.SearchParameters;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.Constants;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.DeckOfCardsEventListener;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.card.Card;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.card.ListCard;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.card.NotificationTextCard;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.card.SimpleTextCard;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.DeckOfCardsManager;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteDeckOfCards;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteDeckOfCardsException;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteResourceStore;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteToqNotification;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.resource.CardImage;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.resource.DeckOfCardsLauncherIcon;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.util.ParcelableUtil;

import org.json.JSONException;

import javax.xml.parsers.ParserConfigurationException;

public class MainActivity extends Activity {

    /** TOQ FIELDS */
    final static String PREFS_FILE= "prefs_file";
    final static String DECK_OF_CARDS_KEY= "deck_of_cards_key";
    final static String DECK_OF_CARDS_VERSION_KEY= "deck_of_cards_version_key";

    boolean notificationSent = false;

    boolean isToqInstalled = false;
    boolean isCardCreated = false;

    boolean firstLoop = true;

    static DeckOfCardsManager mDeckOfCardsManager;
    static RemoteDeckOfCards mRemoteDeckOfCards;
    static RemoteResourceStore mRemoteResourceStore;
    static CardImage[] mCardImages;
    ToqBroadcastReceiver toqReceiver;

    String[] fsmTerms = {"FSM", "Free Speech", "your own view of the Free Speech Movement", "Now", "A Megaphone", "SLATE"};

    static final Map<String, String> termToFigure;
    static
    {
        termToFigure = new HashMap<String, String>();
        termToFigure.put("FSM", "Jack Weinberg");
        termToFigure.put("Free Speech", "Michael Rossman");
        termToFigure.put("your own view of the Free Speech Movement", "Mario Savio");
        termToFigure.put("Now", "Art Goldberg");
        termToFigure.put("A Megaphone", "Joan Baez");
        termToFigure.put("SLATE", "Jackie Goldberg");
    }

    static final Map<String, String> termToImage;
    static
    {
        termToImage = new HashMap<String, String>();
        termToImage.put("FSM", "jack.png");
        termToImage.put("Free Speech", "michael.png");
        termToImage.put("your own view of the Free Speech Movement", "mario.png");
        termToImage.put("Now", "art.png");
        termToImage.put("A Megaphone", "joan.png");
        termToImage.put("SLATE", "jackie.png");
    }

    String[] currentTerm = new String[1];

    /** Trigger watch event when within 50 meters of
      * Latitude: [N] 37.86965 and Longitude: [W] -122.25914 . */
    final float[] sproulLocation = { 37.86965f, -122.25914f };
//    final float[] sproulLocation = { 37.86599277f, -122.26533311f }; //Living Room chair @debug
//    final float[] sproulLocation = { 37.8734602f, -122.2583176f }; //Kresge Library @debug

    final float distanceThresholdMeters = 50f;

    float[] currentLocation = new float[2];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        mDeckOfCardsManager = DeckOfCardsManager.getInstance( getApplicationContext() );
        mDeckOfCardsManager.addDeckOfCardsEventListener( new mDeckOfCardsEventListener() );

        toqReceiver = new ToqBroadcastReceiver();

        isToqInstalled = false;

        initToq();

        initLocationManagement();

        setNewCurrentTerm();

    }

    /**
     * @see android.app.Activity#onStart()
     * This is called after onCreate(Bundle) or after onRestart() if the activity has been stopped
     */
    protected void onStart(){
        super.onStart();

        Log.d(Constants.TAG, "ToqApiDemo.onStart");
        // If not connected, try to connect
        if (!mDeckOfCardsManager.isConnected()){
            try{
                mDeckOfCardsManager.connect();
            }
            catch (RemoteDeckOfCardsException e){
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        installToq();

        if ( firstLoop ) firstLoop = false;
    }

    /************* LOCATION *************/


    private void initLocationManagement() {

        // Acquire a reference to the system Location Manager
        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        // Define a listener that responds to location updates
        LocationListener locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                if ( isToqInstalled ) {
                    // Called when a new location is found by the network location provider.
                    currentLocation[0] = (float) location.getLatitude();
                    currentLocation[1] = (float) location.getLongitude();

                    if (geoDistance(currentLocation, sproulLocation) <= distanceThresholdMeters) {
                        if ( !notificationSent ) {
                            sendNotification( currentTerm );
                            updateFSMTextCard();
                        }
                    } else {
                        setNewCurrentTerm();
                        notificationSent = false;
                    }
//                    Toast.makeText(getApplicationContext(), "Your current geo-distance in meters is: " + Float.toString(geoDistance(currentLocation, sproulLocation)), Toast.LENGTH_SHORT).show();

                }
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {}

            public void onProviderEnabled(String provider) {}

            public void onProviderDisabled(String provider) {}
        };

        // Register the listener with the Location Manager to receive location updates
        if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            // Get update every 5 seconds
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 0, locationListener);
        }

        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            // Get update every 5 seconds
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 0, locationListener);
        }
    }

    private float geoDistance( float[] coord1, float[] coord2 ) {
        float R = 6378.137f; // Radius of earth in KM
        float dLat = (coord2[0] - coord1[0]) * (float)(Math.PI / 180f);
        float dLon = (coord2[1] - coord1[1]) * (float)(Math.PI / 180f);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                   Math.cos(coord1[0] * Math.PI / 180) *
                   Math.cos(coord2[0] * Math.PI / 180) *
                   Math.sin(dLon/2) * Math.sin(dLon/2);
        float c = 2f * (float)Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        float d = R * c;
        return d * 1000; // meters
    }

    private void setNewCurrentTerm() {
        Random generator = new Random();
        int i = generator.nextInt( fsmTerms.length );
        currentTerm[0] = fsmTerms[i];
    }

    public static class GetFlickrActivity extends Activity {

        public GetFlickrActivity() {
            super();
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            Thread thread = new Thread() {
                @Override
                public void run() {
                    try {
                        String svr = "www.flickr.com";

                        REST rest = new REST();
                        rest.setHost(svr);

                        //initialize Flickr object with key and rest
                        Flickr flickr = new Flickr(FlickrHelper.API_KEY, rest);

                        //initialize SearchParameter object, this object stores the search keyword
                        SearchParameters searchParams = new SearchParameters();
                        searchParams.setSort(SearchParameters.INTERESTINGNESS_DESC);

                        //Create tag keyword array
                        String[] tags = new String[]{"cs160fsm"};
                        searchParams.setTags(tags);

                        //Initialize PhotosInterface object
                        PhotosInterface photosInterface = flickr.getPhotosInterface();
                        //Execute search with entered tags
                        PhotoList photoList = photosInterface.search(searchParams, 20, 1);

                        //get search result and fetch the photo object and get small square image's url
                        if (photoList != null) {
                            //Get search result and check the size of photo result
                            Random random = new Random();
                            int seed = random.nextInt(photoList.size());
                            //get photo object
                            Photo photo = (Photo) photoList.get(seed);

                            //Get small square url photo
                            InputStream is = photo.getMediumAsStream();
                            final Bitmap bm = BitmapFactory.decodeStream(is);

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Bitmap resizedBitmap = Bitmap.createScaledBitmap( bm, 250, 288, true );
                                    System.out.println(mCardImages);
                                    System.out.println("MIDDLE");
                                    System.out.println(resizedBitmap);
                                    mCardImages[6] = new CardImage("card.image.flickrPic", resizedBitmap);
                                    mRemoteResourceStore.addResource(mCardImages[6]);
                                    addNewFSMFlickrCard();
                                }
                            });
                        }
                    } catch (ParserConfigurationException e) {
                        e.printStackTrace();
                    } catch (FlickrException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            };

            thread.start();
        }
    }

    /************* TOQ *************/

    private void sendNotification( String[] message ) {
        // Create a NotificationTextCard
        NotificationTextCard notificationCard = new NotificationTextCard( System.currentTimeMillis(),
                                                                          "Draw Request", message );

        // Draw divider between lines of text
        notificationCard.setShowDivider(true);
        // Vibrate to alert user when showing the notification
        notificationCard.setVibeAlert(true);
        // Create a notification with the NotificationTextCard we made
        RemoteToqNotification notification = new RemoteToqNotification( this, notificationCard );

        try {
            // Send the notification
            mDeckOfCardsManager.sendNotification( notification );
            notificationSent = true;
//            Toast.makeText(this, "Sent Notification", Toast.LENGTH_SHORT).show();
        } catch (RemoteDeckOfCardsException e) {
            e.printStackTrace();
//            Toast.makeText( this, "Failed to send Notification", Toast.LENGTH_SHORT ).show();
        }
    }

//

    private void updateFSMTextCard() {

        ListCard listCard = mRemoteDeckOfCards.getListCard();

        SimpleTextCard simpleTextCard = (SimpleTextCard)listCard.childAtIndex(0);
        simpleTextCard.setHeaderText( "Draw " + currentTerm[0] );
        simpleTextCard.setTitleText( termToFigure.get(currentTerm[0]) );
        simpleTextCard.setCardImage( mRemoteResourceStore,
                                     mCardImages[Arrays.asList( fsmTerms ).indexOf( currentTerm[0] )] );

        simpleTextCard.setReceivingEvents(true);
        simpleTextCard.setShowDivider(true);

        try {
            isCardCreated = true;
            mDeckOfCardsManager.updateDeckOfCards(mRemoteDeckOfCards, mRemoteResourceStore);
//            Toast.makeText(this, "Updated SimpleTextCard", Toast.LENGTH_SHORT).show();
        } catch (RemoteDeckOfCardsException e) {
            e.printStackTrace();
//            Toast.makeText(this, "Failed to Update SimpleTextCard", Toast.LENGTH_SHORT).show();
        }
    }

    public static void addNewFSMFlickrCard() {
        ListCard listCard = mRemoteDeckOfCards.getListCard();

        Random generator = new Random();
        int numberOfCards = generator.nextInt( 10000 );

        SimpleTextCard simpleTextCard = new SimpleTextCard( "FlickrCard"
                                                            + Integer.toString( numberOfCards ) );

        simpleTextCard.setHeaderText( "NEW PHOTO!");
        simpleTextCard.setTitleText( "Check out what other people are drawing!" );
        simpleTextCard.setCardImage( mRemoteResourceStore, mCardImages[6] );
        simpleTextCard.setReceivingEvents( true );
        simpleTextCard.setShowDivider( true );

        listCard.add(simpleTextCard);

        try {
            mDeckOfCardsManager.updateDeckOfCards( mRemoteDeckOfCards, mRemoteResourceStore );
            System.out.println( "Created FlickrCard" );
        } catch ( RemoteDeckOfCardsException e ) {
            System.out.println( "Couldn't create FlickrCard" );
            e.printStackTrace();
        }
    }

    private void initToq() {

        // Create the resource store for icons and images
        mRemoteResourceStore= new RemoteResourceStore();

        DeckOfCardsLauncherIcon whiteIcon = null;
        DeckOfCardsLauncherIcon colorIcon = null;

        // Get the launcher icons
        try {
            whiteIcon= new DeckOfCardsLauncherIcon( "white.launcher.icon", getBitmap("bw.png" ), DeckOfCardsLauncherIcon.WHITE );
            colorIcon= new DeckOfCardsLauncherIcon( "color.launcher.icon", getBitmap("color.png" ), DeckOfCardsLauncherIcon.COLOR );
        }
        catch (Exception e) {
            e.printStackTrace();
            System.out.println( "Can't get launcher icon" );
            return;
        }

        /**This is where you store cardImages */
        mCardImages = new CardImage[7];
        try {
            mCardImages[0]= new CardImage("card.image.1", getBitmap("image1.png"));
            for ( int i = 0; i < 6; i++ ) {
                mCardImages[i] = new CardImage( "card.image."+fsmTerms[i],
                                                getBitmap( termToImage.get( fsmTerms[i] ) ) );
            }
        }
        catch ( Exception e ){
            e.printStackTrace();
            System.out.println( "Can't get picture icon" );
            return;
        }

        mRemoteDeckOfCards = createDeckOfCards();

        // Set the custom launcher icons, adding them to the resource store
        mRemoteDeckOfCards.setLauncherIcons( mRemoteResourceStore, new DeckOfCardsLauncherIcon[]{ whiteIcon, colorIcon } );

        // Re-populate the resource store with any card images being used by any of the cards
        for ( Iterator<Card> it= mRemoteDeckOfCards.getListCard().iterator(); it.hasNext(); ) {

            String cardImageId= ( (SimpleTextCard)it.next() ).getCardImageId();

            if ( ( cardImageId != null ) && !mRemoteResourceStore.containsId( cardImageId ) ) {
                if ( cardImageId.equals( "card.image.1" ) ) {
                    mRemoteResourceStore.addResource( mCardImages[0] );
                }
            }
        }
        for ( int i = 1; i < mCardImages.length - 1; i++ ) {
            mRemoteResourceStore.addResource( mCardImages[i] );
        }
    }


    /**
     * Installs applet to Toq watch if app is not yet installed
     */
    private void installToq() {
        boolean isInstalled = true;

        try {
            isInstalled = mDeckOfCardsManager.isInstalled();
            isToqInstalled = true;
        }
        catch (RemoteDeckOfCardsException e) {
            e.printStackTrace();
//            Toast.makeText( this, "Error: Can't determine if app is installed", Toast.LENGTH_SHORT ).show();
        }

        if (!isInstalled) {
            try {
                mDeckOfCardsManager.installDeckOfCards( mRemoteDeckOfCards, mRemoteResourceStore );
                Toast.makeText( this, "App is successfully installed!", Toast.LENGTH_SHORT ).show();
            } catch (RemoteDeckOfCardsException e) {
                e.printStackTrace();
//                Toast.makeText( this, "Error: Cannot install application", Toast.LENGTH_SHORT ).show();
            }
        }
    }

    /** Create some cards with example content */
    private RemoteDeckOfCards createDeckOfCards() {

        ListCard listCard= new ListCard();

        SimpleTextCard simpleTextCard= new SimpleTextCard("card0");
        simpleTextCard.setHeaderText( "Draw " + currentTerm[0] );
        listCard.add(simpleTextCard);

        return new RemoteDeckOfCards( this, listCard );
    }

    /** Read an image from assets and return as a bitmap */
    private Bitmap getBitmap(String fileName) throws Exception {
        try
        {
            InputStream is = getAssets().open(fileName);
            return BitmapFactory.decodeStream(is);
        }
        catch (Exception e)
        {
            throw new Exception("An error occurred getting the bitmap: " + fileName, e);
        }
    }

    public void uninstall(View view) {
        boolean isInstalled = true;

        try {
            isInstalled = mDeckOfCardsManager.isInstalled();
        }
        catch (RemoteDeckOfCardsException e) {
            e.printStackTrace();
//            Toast.makeText(this, "Error: Can't determine if app is installed", Toast.LENGTH_SHORT).show();
        }
        if (isInstalled) {
            try{
                mDeckOfCardsManager.uninstallDeckOfCards();
            }
            catch (RemoteDeckOfCardsException e){
//                Toast.makeText(this, "error_uninstalling_deck_of_cards", Toast.LENGTH_SHORT).show();
            }
        } else {
//            Toast.makeText(this, "already_uninstalled", Toast.LENGTH_SHORT).show();
        }

    }

    /************* EVENT LISTENING TOOLS *************/

    public void startDrawingActivity() {
        Intent i = new Intent(this, DrawingActivity.class);
        startActivity(i);
    }

    public void startDrawingActivity(View view) {
        startDrawingActivity();
    }

    public class mDeckOfCardsEventListener implements DeckOfCardsEventListener {

        public void onCardOpen(java.lang.String s) {
            System.out.println("******************Card Opened*****new Activity*********");
            startDrawingActivity();
        }

        public void onCardVisible(java.lang.String s) {}
        public void onCardInvisible(java.lang.String s) {}
        public void onCardClosed(java.lang.String s) {}
        public void onMenuOptionSelected(java.lang.String s, java.lang.String s1) {}
        public void onMenuOptionSelected(java.lang.String s, java.lang.String s1, java.lang.String s2) {}

    }
}
